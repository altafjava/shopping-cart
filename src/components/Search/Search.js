import React, { Component } from 'react'
import './Search.scss'
import Axios from 'axios'
import { a } from '../Shelf'

class Search extends Component {
    constructor(props) {
        super(props)
        this.state = {
            searchString: ''
        }
    }
    render() {
        return (
            <div className="fixed-header">
                <div className="col-4 col" />
                <div className="col-4 col">
                    <div className="searchbar">
                        <div className="searchbar-icon" style={{ marginTop: "23px" }} />
                        <input
                            className="search-input searchbar-input"
                            placeholder="Search for product"
                            type="text"
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
                <div className="col-4 col" />
            </div>
        )
    }
    handleChange = (e) => {
        this.setState({
            searchString: e.target.value
        })
    }
    handleKeyPress = (target) => {
        if (target.charCode === 13) {
            console.log('searchString=', this.state.searchString)
            Axios.get("http://localhost:8080/search?pageNo=2&q=micro")
                .then(res => {
                    a(res.data)
                })
        }
    }
}

export default Search
