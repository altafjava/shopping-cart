import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchProducts } from '../../services/shelf/actions';
import Spinner from '../Spinner';
import ShelfHeader from './ShelfHeader';
import ProductList from './ProductList';
import './style.scss';
import Axios from 'axios'

class Shelf extends Component {
  constructor(props) {
    super(props)
    this.state = {
      products: []
    }
  }
  static propTypes = {
    fetchProducts: PropTypes.func.isRequired,
    products: PropTypes.array.isRequired,
    filters: PropTypes.array,
    sort: PropTypes.string
  };
  state = {
    isLoading: false
  };
  componentDidMount() {
    // this.handleFetchProducts();
    this.searchProducts();
  }
  componentWillReceiveProps(nextProps) {
    const { filters: nextFilters, sort: nextSort } = nextProps;
    const { filters } = this.props;
    if (nextFilters.length !== filters.length) {
      this.handleFetchProducts(nextFilters, undefined);
    }
    if (nextSort !== this.props.sort) {
      this.handleFetchProducts(undefined, nextSort);
    }
  }
  render() {
    const { products } = this.props;
    const { isLoading } = this.state;
    return (
      <React.Fragment>
        {isLoading && <Spinner />}
        <div className="shelf-container">
          <ShelfHeader productsLength={products.length} />
          <ProductList products={this.state.products} />
        </div>
      </React.Fragment>
    );
  }
  handleFetchProducts = (filters = this.props.filters, sort = this.props.sort) => {
    this.setState({ isLoading: true });
    this.props.fetchProducts(filters, sort, () => {
      this.setState({ isLoading: false });
    });
  };
  searchProducts = () => {
    return Axios.get("http://localhost:8080/search?pageNo=2&q=micro")
      .then(res => {
        this.setState({
          products: res.data
        })
      })
  }
}
const mapStateToProps = state => ({
  products: state.shelf.products,
  filters: state.filters.items,
  sort: state.sort.type
});

export const a = (prd) => {
  console.log('a=', prd)
}
export default connect(mapStateToProps, { fetchProducts })(Shelf);