import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Thumb from '../../../Thumb';
import { addProduct } from '../../../../services/cart/actions';

const Product = ({ product, addProduct }) => {
  product.quantity = 1;
  return (
    <>
      {/* <div className="shelf-item" onClick={() => addProduct(product)} data-sku={product.sku}>
        {product.id && (
          <div className="shelf-stopper">Free shipping</div>
        )}
        <Thumb classes="shelf-item__thumb" src={product.titleImageLink} alt={product.title} />

        <div className="shelf-item__buy-btn">Add to cart</div>
      </div>
      <p className="shelf-item__title">{product.title} mobile 4gb ram</p>
      <div className="shelf-item__price">
        <div className="val">
          <b>{product.discountAmount}</b>
          <small>{product.finalPrice}</small>
          <span>{product.mrp}</span>
        </div>
      </div> */}
      {/* <div className="bhgxx2"> */}
      <div className="card">
        <div className="item">
          <a className="link" target="_blank" rel="noopener noreferrer" href={product.url}>
            <Thumb classes="thumb shelf-item__thumb" src={product.titleImageLink} alt={product.title} />
            <div className="data">
              <div className="col col-8">
                <div>{product.title}</div>
                <div>
                  <ul>
                    {product.keySpecList == null
                      ? false
                      : product.keySpecList.map(element => <li key={element}>{element}</li>)}
                  </ul>
                </div>
              </div>
              <div className="col col-4">
                <div className="price">
                  ₹8,999
                </div>
              </div>
            </div>
          </a>
          {/* </div> */}
        </div>
      </div>
    </>
  );
};
Product.propTypes = { product: PropTypes.object.isRequired, addProduct: PropTypes.func.isRequired };
export default connect(null, { addProduct })(Product);
