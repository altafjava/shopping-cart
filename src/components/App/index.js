import React from 'react';
import Shelf from '../Shelf';
import Filter from '../Shelf/Filter';
import Search from '../Search/Search'

const App = () => (
  <React.Fragment>
    <Search />
    <main>
      <Filter />
      <Shelf />
    </main>
    {/* <FloatCart /> */}
  </React.Fragment>
);

export default App;
